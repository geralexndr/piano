import {
  SET_SEARCH_TERM,
  GET_QUESTIONS,
  GET_ANSWERS,
  SET_QUESTION,
  GET_USERS_QUESTIONS,
  GET_QUESTIONS_BY_TAG
} from "./actions";

export function setSearchTerm(searchTerm) {
  return {
    type: SET_SEARCH_TERM,
    payload: searchTerm
  };
}

export function setQuestion(question) {
  return {
    type: SET_QUESTION,
    payload: question
  };
}

export function setQuestions(questions) {
  return {
    type: GET_QUESTIONS,
    payload: questions
  };
}

export function setAnswers(answers) {
  return {
    type: GET_ANSWERS,
    payload: answers
  };
}

export function setUsersQuestions(usersQuestions) {
  return {
    type: GET_USERS_QUESTIONS,
    payload: usersQuestions
  }
}

export function setQuestionsByTag(questionsByTag) {
  return {
    type: GET_QUESTIONS_BY_TAG,
    payload: questionsByTag
  }
}

export function getQuestions(searchTerm) {
  return dispatch => {
    const url = `https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&site=stackoverflow&body=${searchTerm}`;
    fetch(url)
      .then(res => res.json())
      .then(data => {
        dispatch(setQuestions(data.items));
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export function getAnswers(questionId) {
  return dispatch => {
    const url = `http://api.stackexchange.com/2.2/questions/${questionId}/answers?order=desc&sort=activity&site=stackoverflow`;
    fetch(url)
      .then(res => res.json())
      .then(data => {
        dispatch(setAnswers(data.items));
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export function getUsersQuestions(userId) {
  return dispatch => {
    const url = `http://api.stackexchange.com/2.2/users/${userId}/questions?order=desc&sort=votes&site=stackoverflow`;
    fetch(url)
      .then(res => res.json())
      .then(data => {
        dispatch(setUsersQuestions(data.items));
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export function getQuestionsByTag(tag) {
  return dispatch => {
    const url = `http://api.stackexchange.com/2.2/tags/${tag}/faq?site=stackoverflow`;
    fetch(url)
      .then(res => res.json())
      .then(data => {
        dispatch(setQuestionsByTag(data.items));
      })
      .catch(err => {
        console.log(err);
      });
  };
}
