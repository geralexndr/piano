import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import Search from "./components/Search";
import SearchResults from "./components/SearchResults";
import UsersQuestions from "./components/UsersQuestions";
import QuestionInfo from "./components/QuestionInfo";
import TagResults from "./components/TagResults";
import store from "./store";
import "./App.css";

const FourOhFour = () => <h1>404</h1>;

const App = () => (
  <BrowserRouter>
    <Provider store={store}>
      <div className="container main-app-container">
        <Switch>
          <Route path="/" exact render={Search} />
          <Route path="/results" component={SearchResults} />
          <Route path="/tag/:tag" component={TagResults} />
          <Route path="/answers/:id" component={QuestionInfo} />
          <Route path="/users-qusetions/" component={UsersQuestions} />
          <Route component={FourOhFour} />
        </Switch>
      </div>
    </Provider>
  </BrowserRouter>
);

export default App;
