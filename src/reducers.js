import { combineReducers } from "redux";

import {
  SET_SEARCH_TERM,
  GET_QUESTIONS,
  GET_ANSWERS,
  SET_QUESTION,
  GET_USERS_QUESTIONS,
  GET_QUESTIONS_BY_TAG
} from "./actions";

const searchTerm = (state = "", action) => {
  if (action.type === SET_SEARCH_TERM) {
    return action.payload;
  }
  return state;
};

const question = (state = "", action) => {
  if (action.type === SET_QUESTION) {
    return action.payload;
  }
  return state;
};

const questions = (state = [], action) => {
  if (action.type === GET_QUESTIONS) {
    return action.payload;
  }
  return state;
};

const answers = (state = [], action) => {
  if (action.type === GET_ANSWERS) {
    return action.payload;
  }
  return state;
};

const usersQuestions = (state = [], action) => {
  if (action.type === GET_USERS_QUESTIONS) {
    return action.payload;
  }
  return state;
};

const questionsByTag = (state = [], action) => {
  if (action.type === GET_QUESTIONS_BY_TAG) {
    return action.payload;
  }
  return state;
};

const rootReducer = combineReducers({
  searchTerm,
  questions,
  answers,
  question,
  usersQuestions,
  questionsByTag
});

export default rootReducer;
