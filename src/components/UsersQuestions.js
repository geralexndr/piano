import React, { Component } from "react";
import { connect } from "react-redux";
import Question from "./Question";
import { CSSTransitionGroup } from "react-transition-group";

const QuestionsChart = props => {
  if (props.usersQuestions && props.usersQuestions.length > 0) {
    return props.usersQuestions
      .sort((a, b) => (a.score < b.score ? 1 : -1))
      .map(usersQuestion => (
        <Question
          setQuestion={props.setQuestion}
          getAnswers={props.getAnswers}
          key={usersQuestion.question_id}
          title={usersQuestion.title}
          {...usersQuestion}
        />
      ));
  } else {
    return (
      <tr>
        <td>nothing</td>
      </tr>
    );
  }
};

class UsersQuestions extends Component {
  render() {
    return (
      <div className="col-sm-12">
        <div className="popular-questions">
          <CSSTransitionGroup
            transitionName="example"
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={false}
            transitionLeave={false}
          >
            <table key="authors-questions-table" className="table table-hover">
              <thead>
                <tr>
                  <th>Authors questions</th>
                  <th>Answered</th>
                </tr>
              </thead>
              <tbody>
                <QuestionsChart
                  setQuestion={this.props.setQuestion}
                  getAnswers={this.props.getAnswers}
                  usersQuestions={this.props.usersQuestions}
                  {...this.props}
                />
              </tbody>
            </table>
          </CSSTransitionGroup>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  usersQuestions: state.usersQuestions
});

export default connect(mapStateToProps)(UsersQuestions);
