import React, { Component } from "react";
import { connect } from "react-redux";
import QuestionDetails from "./QuestionDetails";

const TableContent = props => {
  if (props.questions && props.questions.length > 0) {
    return props.questions
      .sort((a, b) => (a.answer_count < b.answer_count ? 1 : -1))
      .map(question => (
        <QuestionDetails
          key={question.question_id}
          question={question}
        />
      ));
  } else {
    return (
      <tr>
        <td />
        <td>no questions</td>
        <td />
        <td />
      </tr>
    );
  }
};

class Questions extends Component {
  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Author</th>
            <th>Theme</th>
            <th>Answered</th>
            <th>Tags</th>
          </tr>
        </thead>
        <tbody>
          <TableContent
            questions={this.props.questions}
          />
        </tbody>
      </table>
    );
  }
}

const mapStateToProps = state => ({
  questions: state.questions
});

export default connect(mapStateToProps)(Questions);
