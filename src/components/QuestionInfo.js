import React, { Component } from "react";
import { connect } from "react-redux";
import { CSSTransitionGroup } from "react-transition-group";

const Answers = props => {
  if (props.answers && props.answers.length > 0) {
    return props.answers
      .sort((a, b) => (a.score < b.score ? 1 : -1))
      .map(answer => {
        const link = `https://stackoverflow.com/a/${answer.answer_id}`;
        return (
          <tr key={answer.answer_id}>
            <td>
              <img
                className="userpic"
                src={answer.owner.profile_image}
                alt="userpic"
              />{" "}
            </td>
            <td>
              <a target="_blank" href={answer.owner.link}>
                {answer.owner.display_name}
              </a>
            </td>
            <td>{answer.score}</td>
            <td>
              <a href={link} target="_blank">
                View answer
              </a>
            </td>
          </tr>
        );
      });
  } else {
    return (
      <tr>
        <td>No answers</td>
      </tr>
    );
  }
};

const QuestionIs = props => {
  if (props.question && props.question !== "") {
    return <h2>{props.question}</h2>;
  } else {
    return <h2>No question selected</h2>;
  }
};

class QuestionInfo extends Component {
  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div key="answers" className="question-info col-sm-12">
          <QuestionIs question={this.props.question} />
          <table className="table table-hover">
            <thead>
              <tr>
                <th>Userpic</th>
                <th>Owner name</th>
                <th>Score</th>
                <th>Answer</th>
              </tr>
            </thead>
            <tbody>
              <Answers answers={this.props.answers} />
            </tbody>
          </table>
        </div>
      </ CSSTransitionGroup>
    );
  }
}

const mapStateToProps = state => ({
  answers: state.answers,
  question: state.question
});

export default connect(mapStateToProps)(QuestionInfo);
