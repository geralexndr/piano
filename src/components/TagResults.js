import React from "react";
import QuestionsByTag from "./QuestionsByTag";
import { CSSTransitionGroup } from "react-transition-group";

const TagResults = () => (
  <div className="container result-screen">
    <div className="col-sm-12">
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <QuestionsByTag key="tagresults" />
      </CSSTransitionGroup>
    </div>
  </div>
);

export default TagResults;
