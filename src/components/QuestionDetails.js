import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getAnswers, setQuestion, getUsersQuestions } from "../actionCreators";
import QuestionTag from "./QuestionTag";

class QuestionDetails extends Component {  
  constructor() {
    super();
    this.showUserQuestions = this.showUserQuestions.bind(this);
    this.viewAnswers = this.viewAnswers.bind(this);
  }

  showUserQuestions() {
    this.props.fetchUsersQuestions(this.props.question.owner.user_id);
    this.props.history.push(
      `/users-qusetions/${this.props.question.owner.user_id}`
    );
  }

  viewAnswers() {
    this.props.fetchAnswers(this.props.question.question_id);
    this.props.setQuestionHeader(this.props.question.title);
    this.props.history.push(`/answers/${this.props.question.question_id}`);
  }

  render() {
    return (
      <tr>
        <td className="question-author">
          <span className="link" onClick={this.showUserQuestions}>
            {this.props.question.owner.display_name}
          </span>
        </td>
        <td>
          <span className="link" onClick={this.viewAnswers}>
            {this.props.question.title}
          </span>
        </td>
        <td>{this.props.question.answer_count}</td>
        <td>
          {this.props.question.tags.map(tag => (
            <QuestionTag
              getQuestionsByTag={this.props.getQuestionsByTag}
              key={Math.random()}
              tag={tag}
            />
          ))}
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchUsersQuestions(userId) {
    dispatch(getUsersQuestions(userId));
  },
  fetchAnswers(questionId) {
    dispatch(getAnswers(questionId));
  },
  setQuestionHeader(question) {
    dispatch(setQuestion(question));
  }
});

export default withRouter(connect(null, mapDispatchToProps)(QuestionDetails));
