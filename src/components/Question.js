import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getAnswers, setQuestion } from "../actionCreators";

class Question extends Component {
  constructor() {
    super();
    this.showQuestionInfo = this.showQuestionInfo.bind(this);
  }

  showQuestionInfo() {
    this.props.fetchAnswers(this.props.question_id);
    this.props.setQuestionHeader(this.props.title);
    this.props.history.push(`/answers/${this.props.question_id}`);
  }

  render() {
    return (
      <tr>
        <td>
          <span className="link" onClick={this.showQuestionInfo}>
            {this.props.title}
          </span>
        </td>
        <td>{this.props.answer_count}</td>
      </tr>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchAnswers(questionId) {
    dispatch(getAnswers(questionId));
  },
  setQuestionHeader(question) {
    dispatch(setQuestion(question));
  }
});

export default withRouter(connect(null, mapDispatchToProps)(Question));
