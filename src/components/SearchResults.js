import React from "react";
import Questions from "./Questions";
import { CSSTransitionGroup } from "react-transition-group";

const SearchResults = () => (
  <CSSTransitionGroup
    transitionName="example"
    transitionAppear={true}
    transitionAppearTimeout={500}
    transitionEnter={false}
    transitionLeave={false}
  >
    <div key="key-123" className="container result-screen">
      <div className="col-sm-12">
        <Questions />
      </div>
    </div>
  </CSSTransitionGroup>
);

export default SearchResults;
