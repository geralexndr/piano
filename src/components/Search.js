import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { setSearchTerm, getQuestions } from "../actionCreators";
import { CSSTransitionGroup } from "react-transition-group";

class Search extends Component {
  constructor() {
    super();
    this.searchFormSubmit = this.searchFormSubmit.bind(this);
  }

  searchFormSubmit(event) {
    event.preventDefault();
    this.props.fetchQuestions(this.props.searchTerm);
    this.props.history.push("/results");
  }

  render() {
    return (
      <CSSTransitionGroup
        transitionName="example"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnter={false}
        transitionLeave={false}
      >
        <div key="key">
          <h1>StackOverflow Search</h1>
          <form onSubmit={this.searchFormSubmit}>
            <div className="input-group jumbotron">
              <input
                onChange={this.props.handleSearchTermChange}
                type="text"
                className="form-control"
                placeholder="Search"
              />
              <div className="input-group-btn">
                <button className="btn btn-success" type="submit">
                  find
                </button>
              </div>
            </div>
          </form>
        </div>
      </CSSTransitionGroup>
    );
  }
}

const mapStateToProps = state => ({
  searchTerm: state.searchTerm
});

const mapDispatchToProps = dispatch => ({
  handleSearchTermChange(event) {
    dispatch(setSearchTerm(event.target.value));
  },
  fetchQuestions(searchTerm) {
    dispatch(getQuestions(searchTerm));
  }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Search));
