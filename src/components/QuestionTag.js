import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getQuestionsByTag } from "../actionCreators";

class QuestionTag extends Component {
  constructor() {
    super();
    this.getTagInfo = this.getTagInfo.bind(this);
  }
  getTagInfo() {
    this.props.setQuestionsByTag(this.props.tag);
    this.props.history.push(`/tag/${this.props.tag}`);
  }
  render() {
    return (
      <span
        onClick={this.getTagInfo}
        className={`label label-primary question-tag`}
      >
        {this.props.tag}
      </span>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  setQuestionsByTag(tag) {
    dispatch(getQuestionsByTag(tag));
  }
});
export default withRouter(connect(null, mapDispatchToProps)(QuestionTag));
