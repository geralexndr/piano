export const demoUsersQuestions = {
  items: [
    {
      tags: ["reactjs", "jsx"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 18,
      answer_count: 2,
      score: 1,
      last_activity_date: 1509610991,
      creation_date: 1509608408,
      question_id: 47069975,
      link:
        "https://stackoverflow.com/questions/47069975/react-class-defination-vs-export-default",
      title: "React Class defination vs export default"
    },
    {
      tags: ["c#", "win-universal-app", "windows-10-universal", "template10"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 343,
      accepted_answer_id: 36247422,
      answer_count: 1,
      score: 1,
      last_activity_date: 1487237457,
      creation_date: 1459080993,
      question_id: 36247206,
      link:
        "https://stackoverflow.com/questions/36247206/template-10-multiple-windows",
      title: "Template 10 Multiple Windows"
    },
    {
      tags: ["prolog", "primes"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 4469,
      accepted_answer_id: 12446593,
      answer_count: 2,
      score: 1,
      last_activity_date: 1474744169,
      creation_date: 1347794139,
      last_edit_date: 1474744169,
      question_id: 12446407,
      link:
        "https://stackoverflow.com/questions/12446407/prolog-find-n-prime-numbers",
      title: "Prolog Find N prime numbers"
    },
    {
      tags: [
        "ruby-on-rails",
        "heroku",
        "amazon-web-services",
        "amazon-s3",
        "jquery-file-upload"
      ],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 576,
      accepted_answer_id: 31631042,
      answer_count: 3,
      score: 1,
      last_activity_date: 1470581532,
      creation_date: 1422530773,
      question_id: 28213554,
      link:
        "https://stackoverflow.com/questions/28213554/ruby-on-rails-direct-aws-s3-upload-with-jqueryfileupload",
      title: "Ruby on Rails Direct AWS S3 Upload with JqueryFileUpload"
    },
    {
      tags: ["list", "character-encoding", "prolog", "character"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 7914,
      accepted_answer_id: 13670956,
      answer_count: 3,
      score: 5,
      last_activity_date: 1462269287,
      creation_date: 1354462818,
      last_edit_date: 1390696611,
      question_id: 13670798,
      link:
        "https://stackoverflow.com/questions/13670798/prolog-list-of-charcodes-to-a-string-or-characters",
      title: "Prolog - List of CharCodes to a String or Characters"
    },
    {
      tags: ["javascript", "reactjs", "datepicker", "calendar", "material-ui"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 525,
      accepted_answer_id: 35408783,
      answer_count: 1,
      score: 0,
      last_activity_date: 1455541098,
      creation_date: 1455356155,
      last_edit_date: 1455541098,
      question_id: 35378272,
      link:
        "https://stackoverflow.com/questions/35378272/render-full-calendar-of-datepicker-on-a-page",
      title: "Render Full Calendar of DatePicker on a page"
    },
    {
      tags: ["ruby-on-rails", "arrays", "ruby", "ruby-on-rails-4"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 207,
      accepted_answer_id: 31138459,
      answer_count: 2,
      score: 2,
      last_activity_date: 1435668074,
      creation_date: 1435667277,
      last_edit_date: 1435667860,
      question_id: 31138328,
      link:
        "https://stackoverflow.com/questions/31138328/rails-undefined-method-for-nilnilclass",
      title: "Rails undefined method `[]&#39; for nil:NilClass"
    },
    {
      tags: ["c#", "json", "instagram", "json.net"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 6759,
      accepted_answer_id: 13730121,
      answer_count: 2,
      score: 2,
      last_activity_date: 1415891362,
      creation_date: 1354732259,
      last_edit_date: 1354732842,
      question_id: 13729997,
      link:
        "https://stackoverflow.com/questions/13729997/retrieving-images-from-a-json-string-retrived-from-instagram-api",
      title:
        "Retrieving &quot;images&quot; from a JSON string retrived from Instagram API"
    },
    {
      tags: ["haskell", "compiler-errors", "syntax-error"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 89,
      closed_date: 1354504687,
      accepted_answer_id: 13663983,
      answer_count: 1,
      score: -1,
      last_activity_date: 1402505892,
      creation_date: 1354395948,
      last_edit_date: 1402505892,
      question_id: 13663730,
      link:
        "https://stackoverflow.com/questions/13663730/haskell-printing-and-returning-a-state-at-the-same-time",
      closed_reason: "not a real question",
      title: "Haskell - Printing and returning a state at the same time"
    },
    {
      tags: ["prolog", "return-value"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 283,
      answer_count: 1,
      score: 1,
      last_activity_date: 1391456520,
      creation_date: 1354457772,
      last_edit_date: 1495540573,
      question_id: 13670106,
      link:
        "https://stackoverflow.com/questions/13670106/prolog-not-returning-desired-variable",
      title: "Prolog - Not returning desired Variable"
    },
    {
      tags: ["prolog", "return-value"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 148,
      accepted_answer_id: 13670677,
      answer_count: 1,
      score: 3,
      last_activity_date: 1391456373,
      creation_date: 1354460327,
      last_edit_date: 1391456373,
      question_id: 13670453,
      link:
        "https://stackoverflow.com/questions/13670453/prolog-not-returning-desired-value",
      title: "Prolog - Not returning desired value"
    },
    {
      tags: ["haskell", "types", "typeerror"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 88,
      accepted_answer_id: 13664283,
      answer_count: 2,
      score: 0,
      last_activity_date: 1387814512,
      creation_date: 1354399208,
      last_edit_date: 1387814512,
      question_id: 13664153,
      link:
        "https://stackoverflow.com/questions/13664153/haskell-a-specific-type-error-with-my-own-data-type",
      title: "Haskell - A specific type error with my own data type"
    },
    {
      tags: ["parallel-processing", "mpi", "distributed-computing"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 346,
      accepted_answer_id: 16451381,
      answer_count: 1,
      score: 1,
      last_activity_date: 1376322948,
      creation_date: 1367518127,
      last_edit_date: 1376322948,
      question_id: 16344848,
      link:
        "https://stackoverflow.com/questions/16344848/will-mpi-send-block-if-matching-mpi-irecv-takes-less-data-elements",
      title:
        "Will MPI_Send block if matching MPI_IRecv takes less data elements?"
    },
    {
      tags: ["web-services", "web-applications", "printing"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 2459,
      accepted_answer_id: 18024705,
      answer_count: 1,
      score: 2,
      last_activity_date: 1375471447,
      creation_date: 1375470883,
      question_id: 18024545,
      link:
        "https://stackoverflow.com/questions/18024545/can-web-applications-detect-local-printers",
      title: "Can Web Applications detect local printers?"
    },
    {
      tags: ["c#", ".net", "performance", "dictionary"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 855,
      accepted_answer_id: 17709304,
      answer_count: 4,
      score: 0,
      last_activity_date: 1374092126,
      creation_date: 1374091701,
      last_edit_date: 1374092012,
      question_id: 17709261,
      link:
        "https://stackoverflow.com/questions/17709261/dictionary-trygetvalue-without-using-value-returned",
      title: "Dictionary TryGetValue without using value returned"
    },
    {
      tags: ["java", "concurrency", "monitors"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 166,
      answer_count: 2,
      score: 0,
      last_activity_date: 1367514331,
      creation_date: 1367510274,
      last_edit_date: 1367514331,
      question_id: 16342571,
      link:
        "https://stackoverflow.com/questions/16342571/notifyall-not-the-last-statement",
      title: "notifyall() not the last statement"
    },
    {
      tags: ["c", "parallel-processing", "mpi", "distributed-computing"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 187,
      accepted_answer_id: 16133674,
      answer_count: 1,
      score: 1,
      last_activity_date: 1366563124,
      creation_date: 1366558617,
      question_id: 16132899,
      link:
        "https://stackoverflow.com/questions/16132899/can-more-than-1-process-use-mpi-scatter",
      title: "Can more than 1 process use MPI_Scatter?"
    },
    {
      tags: ["c++", "visual-studio-2010", "visual-studio", "visual-c++"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 327,
      accepted_answer_id: 15356809,
      answer_count: 2,
      score: 0,
      last_activity_date: 1363079168,
      creation_date: 1363078159,
      question_id: 15356771,
      link:
        "https://stackoverflow.com/questions/15356771/compile-error-with-typedef-and-std",
      title: "Compile Error with typedef and std"
    },
    {
      tags: ["java", "perl", "http", "unix", "web-applications"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 357,
      accepted_answer_id: 15323702,
      answer_count: 1,
      score: 1,
      last_activity_date: 1362928154,
      creation_date: 1362860883,
      question_id: 15315542,
      link:
        "https://stackoverflow.com/questions/15315542/writing-to-a-perl-process-input-stream-in-java",
      title: "Writing to a Perl Process Input Stream in Java"
    },
    {
      tags: ["haskell", "infinite-sequence"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 311,
      accepted_answer_id: 13549853,
      answer_count: 3,
      score: 1,
      last_activity_date: 1353840826,
      creation_date: 1353836177,
      question_id: 13549819,
      link:
        "https://stackoverflow.com/questions/13549819/haskell-negating-even-numbers-in-infinite-stream",
      title: "Haskell - Negating even numbers in infinite stream"
    },
    {
      tags: ["haskell", "power-series"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 1146,
      accepted_answer_id: 12998779,
      answer_count: 2,
      score: 5,
      last_activity_date: 1351036695,
      creation_date: 1350829026,
      question_id: 12998635,
      link:
        "https://stackoverflow.com/questions/12998635/power-series-in-haskell",
      title: "Power Series in Haskell"
    },
    {
      tags: ["c++", "visual-studio-2010", "visual-c++"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 1621,
      accepted_answer_id: 13010239,
      answer_count: 3,
      score: 3,
      last_activity_date: 1350905346,
      creation_date: 1350902861,
      question_id: 13009815,
      link:
        "https://stackoverflow.com/questions/13009815/visual-studio-c-able-to-compile-with-compile-errors-red-underlines",
      title:
        "Visual Studio C++ able to compile with compile errors (red underlines)"
    },
    {
      tags: ["haskell", "fractions", "rational-numbers"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 1276,
      accepted_answer_id: 13000030,
      answer_count: 1,
      score: 0,
      last_activity_date: 1350838814,
      creation_date: 1350836374,
      last_edit_date: 1495541039,
      question_id: 12999683,
      link:
        "https://stackoverflow.com/questions/12999683/using-fractions-in-haskell",
      title: "Using Fractions in Haskell"
    },
    {
      tags: [
        "haskell",
        "lambda",
        "anonymous-function",
        "higher-order-functions"
      ],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 660,
      accepted_answer_id: 12883591,
      answer_count: 2,
      score: 1,
      last_activity_date: 1350229732,
      creation_date: 1350228047,
      question_id: 12883559,
      link:
        "https://stackoverflow.com/questions/12883559/guards-and-concatiating-to-lists-in-an-anonymous-function",
      title: "Guards and concatiating to lists in an anonymous function"
    },
    {
      tags: ["list", "haskell", "lambda", "higher-order-functions"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 358,
      accepted_answer_id: 12883417,
      answer_count: 1,
      score: 0,
      last_activity_date: 1350227010,
      creation_date: 1350226496,
      question_id: 12883358,
      link:
        "https://stackoverflow.com/questions/12883358/concatenating-elements-into-a-list-in-a-anonymous-function-in-haskell",
      title:
        "Concatenating elements into a list in a anonymous function in Haskell"
    },
    {
      tags: ["algorithm"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 989,
      accepted_answer_id: 5156239,
      answer_count: 3,
      score: 5,
      last_activity_date: 1348619033,
      creation_date: 1298991514,
      last_edit_date: 1348619033,
      question_id: 5156161,
      link:
        "https://stackoverflow.com/questions/5156161/divide-and-conquer-plural-array",
      title: "Divide and conquer - Plural Array"
    },
    {
      tags: ["sql", "database", "performance", "ms-access"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: false,
      view_count: 90,
      answer_count: 0,
      score: 1,
      last_activity_date: 1307596098,
      creation_date: 1307596098,
      question_id: 6288333,
      link:
        "https://stackoverflow.com/questions/6288333/microsoft-access-2007-lags-after-print-job",
      title: "Microsoft Access 2007 Lags after Print Job"
    },
    {
      tags: ["sql", "ms-access", "query-optimization"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 4718,
      accepted_answer_id: 6183595,
      answer_count: 3,
      score: 1,
      last_activity_date: 1306873428,
      creation_date: 1306819576,
      last_edit_date: 1306820546,
      question_id: 6183341,
      link:
        "https://stackoverflow.com/questions/6183341/optimizing-not-in-query-in-access-sql",
      title: "Optimizing NOT IN query in Access SQL"
    },
    {
      tags: ["java", "audio", "mouseevent", "javasound"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 1462,
      accepted_answer_id: 4124588,
      answer_count: 3,
      score: 0,
      last_activity_date: 1305894705,
      creation_date: 1289223788,
      last_edit_date: 1305894309,
      question_id: 4124358,
      link:
        "https://stackoverflow.com/questions/4124358/preloading-the-sourcedataline-to-reduce-lag",
      title: "Preloading the SourceDataLine to reduce lag"
    },
    {
      tags: ["c#", "asp.net", "sql", "visual-studio-2010", "dynamic-sql"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 2670,
      accepted_answer_id: 3942725,
      answer_count: 4,
      score: 4,
      last_activity_date: 1287149874,
      creation_date: 1287148947,
      last_edit_date: 1287149190,
      question_id: 3942612,
      link:
        "https://stackoverflow.com/questions/3942612/new-to-dynamic-sql-statements",
      title: "New to Dynamic SQL Statements"
    }
  ],
  has_more: false,
  quota_max: 10000,
  quota_remaining: 9675
};

export const demoQuestions = {
  items: [
    {
      tags: ["reactjs", "jsx"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 12,
      answer_count: 2,
      score: 1,
      last_activity_date: 1509609319,
      creation_date: 1509608408,
      question_id: 47069975,
      link:
        "https://stackoverflow.com/questions/47069975/react-class-defination-vs-export-default",
      title: "React Class defination vs export default"
    },
    {
      tags: ["javascript", "html", "css", "reactjs", "semantic-ui"],
      owner: {
        reputation: 942,
        user_id: 2653120,
        user_type: "registered",
        accept_rate: 78,
        profile_image: "https://i.stack.imgur.com/MEltB.jpg?s=128&g=1",
        display_name: "amir",
        link: "https://stackoverflow.com/users/2653120/amir"
      },
      is_answered: false,
      view_count: 9,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509609301,
      creation_date: 1509609301,
      question_id: 47070207,
      link:
        "https://stackoverflow.com/questions/47070207/how-to-set-min-height-in-semantic-ui-react",
      title: "how to set min height in semantic-ui react"
    },
    {
      tags: ["java", "appium-ios"],
      owner: {
        reputation: 1,
        user_id: 8867435,
        user_type: "registered",
        profile_image:
          "https://www.gravatar.com/avatar/ce768445e3d060eb1590cf1c7a5ae323?s=128&d=identicon&r=PG&f=1",
        display_name: "Shiv",
        link: "https://stackoverflow.com/users/8867435/shiv"
      },
      is_answered: false,
      view_count: 18,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509609147,
      creation_date: 1509532328,
      last_edit_date: 1509532660,
      question_id: 47052808,
      link:
        "https://stackoverflow.com/questions/47052808/unable-to-access-ios-app-custom-view-elements-using-appium",
      title: "Unable to access ios app custom view elements using appium"
    },
    {
      tags: ["forms", "reactjs", "web", "semantic-ui"],
      owner: {
        reputation: 47,
        user_id: 5726764,
        user_type: "registered",
        accept_rate: 46,
        profile_image:
          "https://graph.facebook.com/10206317377145559/picture?type=large",
        display_name: "Eran Abir",
        link: "https://stackoverflow.com/users/5726764/eran-abir"
      },
      is_answered: false,
      view_count: 5,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509609125,
      creation_date: 1509609125,
      question_id: 47070165,
      link:
        "https://stackoverflow.com/questions/47070165/semantic-react-form-select-how-to-use-custom-array-options",
      title: "Semantic React - Form.Select how to use custom array options"
    },
    {
      tags: ["reactjs"],
      owner: {
        reputation: 1163,
        user_id: 4192691,
        user_type: "registered",
        accept_rate: 32,
        profile_image: "https://i.stack.imgur.com/dzokh.jpg?s=128&g=1",
        display_name: "aryan",
        link: "https://stackoverflow.com/users/4192691/aryan"
      },
      is_answered: true,
      view_count: 10525,
      accepted_answer_id: 34893582,
      answer_count: 4,
      score: 9,
      last_activity_date: 1509609097,
      creation_date: 1453273393,
      last_edit_date: 1453308627,
      question_id: 34893506,
      link:
        "https://stackoverflow.com/questions/34893506/return-multiple-elements-inside-react-render",
      title: "Return multiple elements inside React.render()"
    },
    {
      tags: ["reactjs", "google-maps", "typescript", "webpack"],
      owner: {
        reputation: 355,
        user_id: 1875786,
        user_type: "registered",
        accept_rate: 65,
        profile_image: "https://i.stack.imgur.com/3c4zt.jpg?s=128&g=1",
        display_name: "ThunderDev",
        link: "https://stackoverflow.com/users/1875786/thunderdev"
      },
      is_answered: false,
      view_count: 17,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509608999,
      creation_date: 1509534716,
      question_id: 47053526,
      link:
        "https://stackoverflow.com/questions/47053526/how-to-use-google-maps-api-with-webpack-and-typescript",
      title: "How to use Google maps api with webpack and typescript"
    },
    {
      tags: ["javascript", "reactjs"],
      owner: {
        reputation: 77,
        user_id: 4687330,
        user_type: "registered",
        accept_rate: 100,
        profile_image:
          "https://www.gravatar.com/avatar/938e243b884a74beef886df8bdd2526a?s=128&d=identicon&r=PG&f=1",
        display_name: "Rakesh Nallam",
        link: "https://stackoverflow.com/users/4687330/rakesh-nallam"
      },
      is_answered: false,
      view_count: 21,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509608673,
      creation_date: 1509608094,
      last_edit_date: 1509608267,
      question_id: 47069892,
      link:
        "https://stackoverflow.com/questions/47069892/creating-a-common-function",
      title: "creating a common function"
    },
    {
      tags: [
        "javascript",
        "google-chrome",
        "google-chrome-extension",
        "local-storage"
      ],
      owner: {
        reputation: 1968,
        user_id: 954930,
        user_type: "registered",
        accept_rate: 90,
        profile_image: "https://i.stack.imgur.com/1pEmC.png?s=128&g=1",
        display_name: "ItsGreg",
        link: "https://stackoverflow.com/users/954930/itsgreg"
      },
      is_answered: false,
      view_count: 12,
      answer_count: 0,
      score: 1,
      last_activity_date: 1509608589,
      creation_date: 1509608589,
      question_id: 47070011,
      link:
        "https://stackoverflow.com/questions/47070011/editing-localstorage-with-a-chrome-extension",
      title: "Editing localStorage with a Chrome Extension?"
    },
    {
      tags: ["react-native", "react-native-ios"],
      owner: {
        reputation: 1,
        user_id: 8306887,
        user_type: "registered",
        profile_image:
          "https://www.gravatar.com/avatar/a5971f68a00a1aaba27e8ee6d5fc422b?s=128&d=identicon&r=PG&f=1",
        display_name: "paprika zhj",
        link: "https://stackoverflow.com/users/8306887/paprika-zhj"
      },
      is_answered: false,
      view_count: 6,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509608554,
      creation_date: 1509606635,
      last_edit_date: 1509608554,
      question_id: 47069528,
      link:
        "https://stackoverflow.com/questions/47069528/network-request-failed-react-native-ios-ios-app-expo",
      title: "network request failed, react native, ios , ios app expo"
    },
    {
      tags: [
        "react-native",
        "react-native-android",
        "react-native-ios",
        "native-base"
      ],
      owner: {
        reputation: 97,
        user_id: 3386421,
        user_type: "registered",
        accept_rate: 27,
        profile_image:
          "https://www.gravatar.com/avatar/985f9bd0315261bf8f86bd46661a1595?s=128&d=identicon&r=PG&f=1",
        display_name: "neo",
        link: "https://stackoverflow.com/users/3386421/neo"
      },
      is_answered: true,
      view_count: 29,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509607804,
      creation_date: 1509511254,
      last_edit_date: 1509607804,
      question_id: 47048352,
      link:
        "https://stackoverflow.com/questions/47048352/how-to-import-nativebase-2-2-0-to-reactnative-project",
      title: "How to import NativeBase 2.2.0 to ReactNative project?"
    },
    {
      tags: ["electron", "create-react-app"],
      owner: {
        reputation: 59,
        user_id: 2201169,
        user_type: "registered",
        accept_rate: 57,
        profile_image:
          "https://www.gravatar.com/avatar/fa0dd3f6edcbdad3ca7a196dc7943a21?s=128&d=identicon&r=PG",
        display_name: "Thomas",
        link: "https://stackoverflow.com/users/2201169/thomas"
      },
      is_answered: false,
      view_count: 5,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509607468,
      creation_date: 1509607468,
      question_id: 47069738,
      link:
        "https://stackoverflow.com/questions/47069738/should-i-disable-create-react-app-serviceworker-in-electron-app",
      title: "Should I disable create-react-app serviceWorker in Electron app?"
    },
    {
      tags: ["javascript", "automated-tests", "nightmare"],
      owner: {
        reputation: 153,
        user_id: 7047593,
        user_type: "registered",
        profile_image:
          "https://lh4.googleusercontent.com/-KQNjtkTArcI/AAAAAAAAAAI/AAAAAAAAAqY/Y17MVhVjtB0/photo.jpg?sz=128",
        display_name: "JiN",
        link: "https://stackoverflow.com/users/7047593/jin"
      },
      is_answered: false,
      view_count: 15,
      answer_count: 0,
      score: -2,
      last_activity_date: 1509606976,
      creation_date: 1509601352,
      last_edit_date: 1509606976,
      question_id: 47068344,
      link:
        "https://stackoverflow.com/questions/47068344/nightmare-js-webpage-isnt-loading-network-calls-are-pending",
      title: "Nightmare JS webpage isn&#39;t loading. Network calls are pending"
    },
    {
      tags: ["javascript", "button", "react-native", "styles"],
      owner: {
        reputation: 1,
        user_id: 8132240,
        user_type: "registered",
        profile_image:
          "https://graph.facebook.com/10211802315805317/picture?type=large",
        display_name: "wein",
        link: "https://stackoverflow.com/users/8132240/wein"
      },
      is_answered: false,
      view_count: 13,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509606324,
      creation_date: 1509606324,
      question_id: 47069453,
      link:
        "https://stackoverflow.com/questions/47069453/button-change-style-after-onpress-with-react-native",
      title: "Button change style after onPress with React Native"
    },
    {
      tags: ["javascript", "ajax", "reactjs", "jsp", "react-redux"],
      owner: {
        reputation: 26,
        user_id: 2119252,
        user_type: "registered",
        accept_rate: 20,
        profile_image:
          "https://www.gravatar.com/avatar/3354defede58354a84badfc0f0b26302?s=128&d=identicon&r=PG",
        display_name: "user2119252",
        link: "https://stackoverflow.com/users/2119252/user2119252"
      },
      is_answered: false,
      view_count: 7,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509606055,
      creation_date: 1509606055,
      question_id: 47069390,
      link:
        "https://stackoverflow.com/questions/47069390/how-to-retrieve-url-params-or-session-object-from-jsp-to-reactjs",
      title: "How to retrieve url params or session object from jsp to reactjs"
    },
    {
      tags: ["javascript", "reactjs", "meteor", "translation", "meteor-react"],
      owner: {
        reputation: 3,
        user_id: 8010629,
        user_type: "registered",
        profile_image:
          "https://www.gravatar.com/avatar/733ad94a26e09f95b2db374bef010708?s=128&d=identicon&r=PG&f=1",
        display_name: "Amit Akolkar",
        link: "https://stackoverflow.com/users/8010629/amit-akolkar"
      },
      is_answered: false,
      view_count: 5,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509605653,
      creation_date: 1509605653,
      question_id: 47069276,
      link:
        "https://stackoverflow.com/questions/47069276/i18n-language-translation-in-meteor-react",
      title: "i18n Language Translation in Meteor-React"
    },
    {
      tags: ["reactjs", "redux", "react-redux", "react-router-v4"],
      owner: {
        reputation: 648,
        user_id: 1359708,
        user_type: "registered",
        accept_rate: 44,
        profile_image:
          "https://www.gravatar.com/avatar/2e9b46c5fdc3f3f53b67459e85f9b17c?s=128&d=identicon&r=PG",
        display_name: "vpoola88",
        link: "https://stackoverflow.com/users/1359708/vpoola88"
      },
      is_answered: false,
      view_count: 15,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509605540,
      creation_date: 1509603674,
      last_edit_date: 1509605540,
      question_id: 47068833,
      link:
        "https://stackoverflow.com/questions/47068833/routing-based-on-props-with-react-redux-react-router-4",
      title: "Routing based on Props with React, Redux, React-Router 4"
    },
    {
      tags: ["javascript", "reactjs", "redux", "containers", "dispatch"],
      owner: {
        reputation: 59,
        user_id: 7724099,
        user_type: "registered",
        accept_rate: 100,
        profile_image:
          "https://lh6.googleusercontent.com/-GEcCwc0Ei6k/AAAAAAAAAAI/AAAAAAAACts/FznT3I-R1vA/photo.jpg?sz=128",
        display_name: "Liz Parody",
        link: "https://stackoverflow.com/users/7724099/liz-parody"
      },
      is_answered: true,
      view_count: 37,
      answer_count: 5,
      score: 3,
      last_activity_date: 1509605164,
      creation_date: 1509594214,
      question_id: 47067241,
      link:
        "https://stackoverflow.com/questions/47067241/react-js-in-plain-english-what-is-mapdispatchtoprops",
      title: "React.js - In plain english what is mapDispatchToProps?"
    },
    {
      tags: ["javascript", "reactjs", "typescript", "react-redux"],
      owner: {
        reputation: 362,
        user_id: 604950,
        user_type: "registered",
        accept_rate: 83,
        profile_image: "https://i.stack.imgur.com/T5QbF.jpg?s=128&g=1",
        display_name: "Falieson",
        link: "https://stackoverflow.com/users/604950/falieson"
      },
      is_answered: false,
      view_count: 37,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509605016,
      creation_date: 1509292617,
      last_edit_date: 1509297159,
      question_id: 47002230,
      link:
        "https://stackoverflow.com/questions/47002230/redux-connect-to-component-typescript-dispatchpropsany-error",
      title:
        "redux connect to component typescript DispatchProps&lt;any&gt; error"
    },
    {
      tags: ["twitter-bootstrap", "reactjs", "select", "bootstrap-select"],
      owner: {
        reputation: 31,
        user_id: 6187671,
        user_type: "registered",
        accept_rate: 83,
        profile_image: "https://i.stack.imgur.com/3N3B4.jpg?s=128&g=1",
        display_name: "ashfaqrafi",
        link: "https://stackoverflow.com/users/6187671/ashfaqrafi"
      },
      is_answered: false,
      view_count: 11,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509604548,
      creation_date: 1509602553,
      last_edit_date: 1509604548,
      question_id: 47068603,
      link:
        "https://stackoverflow.com/questions/47068603/bootstrap-select-is-used-in-multiple-places-in-one-page-but-only-working-for-the",
      title:
        "bootstrap-select is used in multiple places in one page but only working for the first one"
    },
    {
      tags: ["variables", "firebase", "react-native", "server"],
      owner: {
        reputation: 21,
        user_id: 8749566,
        user_type: "registered",
        accept_rate: 67,
        profile_image:
          "https://lh4.googleusercontent.com/-pKc2s9aL1eE/AAAAAAAAAAI/AAAAAAAAAAc/acJqfJiEK2o/photo.jpg?sz=128",
        display_name: "GIISE",
        link: "https://stackoverflow.com/users/8749566/giise"
      },
      is_answered: true,
      view_count: 39,
      accepted_answer_id: 46988921,
      answer_count: 4,
      score: 0,
      last_activity_date: 1509604056,
      creation_date: 1509156700,
      last_edit_date: 1509174898,
      question_id: 46985587,
      link:
        "https://stackoverflow.com/questions/46985587/react-native-firebase-cant-find-variable-ref",
      title: "React Native/Firebase can&#39;t find variable &quot;Ref&quot;"
    },
    {
      tags: ["javascript", "reactjs", "react-native", "ecmascript-6"],
      owner: {
        reputation: 91,
        user_id: 6851076,
        user_type: "registered",
        accept_rate: 84,
        profile_image: "https://i.stack.imgur.com/0cDQx.jpg?s=128&g=1",
        display_name: "Shayan Javadi",
        link: "https://stackoverflow.com/users/6851076/shayan-javadi"
      },
      is_answered: false,
      view_count: 13,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509603661,
      creation_date: 1509590826,
      question_id: 47066778,
      link:
        "https://stackoverflow.com/questions/47066778/is-it-possible-to-access-non-static-methods-in-react-navigations-navigationopti",
      title:
        "Is it possible to access non static methods in React Navigation&#39;s navigationOptions?"
    },
    {
      tags: ["react-native"],
      owner: {
        reputation: 1,
        user_id: 4808899,
        user_type: "registered",
        profile_image:
          "https://lh3.googleusercontent.com/-KSAHky0X0L4/AAAAAAAAAAI/AAAAAAAAADE/qbIWt3hTD0M/photo.jpg?sz=128",
        display_name: "Surendra Kumar",
        link: "https://stackoverflow.com/users/4808899/surendra-kumar"
      },
      is_answered: false,
      view_count: 10,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509603139,
      creation_date: 1509602786,
      last_edit_date: 1509603139,
      question_id: 47068658,
      link:
        "https://stackoverflow.com/questions/47068658/how-to-show-carousel-slider-within-image-container-in-react-native-android-app",
      title:
        "How to show carousel slider within image container in react native android app"
    },
    {
      tags: ["reactjs", "react-native", "jestjs"],
      owner: {
        reputation: 21,
        user_id: 8837496,
        user_type: "registered",
        profile_image:
          "https://www.gravatar.com/avatar/383b9fc57001692e46a2d930c1f619c5?s=128&d=identicon&r=PG&f=1",
        display_name: "sAm_vdP",
        link: "https://stackoverflow.com/users/8837496/sam-vdp"
      },
      is_answered: false,
      view_count: 18,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509602834,
      creation_date: 1509551949,
      last_edit_date: 1509602834,
      question_id: 47058905,
      link:
        "https://stackoverflow.com/questions/47058905/using-react-test-renderer-with-switch-causes-stateless-function-components-ca",
      title:
        "Using react-test-renderer with &lt;Switch&gt; causes &quot;Stateless function components cannot be given refs&quot; warning"
    },
    {
      tags: ["reactjs", "typescript"],
      owner: {
        reputation: 285,
        user_id: 7828585,
        user_type: "registered",
        accept_rate: 100,
        profile_image: "https://i.stack.imgur.com/TbVhR.jpg?s=128&g=1",
        display_name: "dpaulus",
        link: "https://stackoverflow.com/users/7828585/dpaulus"
      },
      is_answered: true,
      view_count: 36,
      answer_count: 2,
      score: 2,
      last_activity_date: 1509602710,
      creation_date: 1509474952,
      question_id: 47042392,
      link:
        "https://stackoverflow.com/questions/47042392/joining-react-props-with-custom-props",
      title: "Joining React props with custom props"
    },
    {
      tags: ["mocha", "webdriver-io"],
      owner: {
        reputation: 1,
        user_id: 8866082,
        user_type: "registered",
        profile_image:
          "https://www.gravatar.com/avatar/4a6e9c6c89640da3d4aab95f62e61a96?s=128&d=identicon&r=PG&f=1",
        display_name: "user8866082",
        link: "https://stackoverflow.com/users/8866082/user8866082"
      },
      is_answered: false,
      view_count: 2,
      answer_count: 0,
      score: 0,
      last_activity_date: 1509602386,
      creation_date: 1509602386,
      question_id: 47068556,
      link:
        "https://stackoverflow.com/questions/47068556/webdriverio-mocha-unable-to-click-on-toggle-element",
      title: "WebDriverIO + Mocha + Unable to click on Toggle element"
    },
    {
      tags: ["javascript", "reactjs", "momentjs"],
      owner: {
        reputation: 1,
        user_id: 8870784,
        user_type: "registered",
        profile_image:
          "https://www.gravatar.com/avatar/3366116077908a32d54bd86ff1b8c3be?s=128&d=identicon&r=PG&f=1",
        display_name: "user8870784",
        link: "https://stackoverflow.com/users/8870784/user8870784"
      },
      is_answered: false,
      view_count: 49,
      answer_count: 1,
      score: 0,
      last_activity_date: 1509601345,
      creation_date: 1509573626,
      last_edit_date: 1509574136,
      question_id: 47064380,
      link:
        "https://stackoverflow.com/questions/47064380/momentjs-returns-an-object",
      title: "momentjs returns an object"
    },
    {
      tags: ["neo4j", "reactjs"],
      owner: {
        reputation: 59,
        user_id: 5345145,
        user_type: "registered",
        accept_rate: 45,
        profile_image:
          "https://www.gravatar.com/avatar/d96958557aebccb46d05f5a28ab695c6?s=128&d=identicon&r=PG&f=1",
        display_name: "Dhanya",
        link: "https://stackoverflow.com/users/5345145/dhanya"
      },
      is_answered: true,
      view_count: 971,
      accepted_answer_id: 33102279,
      answer_count: 2,
      score: 2,
      last_activity_date: 1509600600,
      creation_date: 1444731919,
      question_id: 33100248,
      link:
        "https://stackoverflow.com/questions/33100248/using-neo4j-with-react-js",
      title: "Using Neo4j with React JS"
    },
    {
      tags: ["javascript", "reactjs", "react-native"],
      owner: {
        reputation: 1953,
        user_id: 1282896,
        user_type: "registered",
        accept_rate: 73,
        profile_image: "https://i.stack.imgur.com/wVHej.jpg?s=128&g=1",
        display_name: "Egor T",
        link: "https://stackoverflow.com/users/1282896/egor-t"
      },
      is_answered: false,
      view_count: 13,
      answer_count: 0,
      score: 1,
      last_activity_date: 1509600115,
      creation_date: 1509590968,
      last_edit_date: 1509600115,
      question_id: 47066799,
      link:
        "https://stackoverflow.com/questions/47066799/react-native-download-folder-from-url",
      title: "React Native Download folder from url"
    },
    {
      tags: ["react-native"],
      owner: {
        reputation: 608,
        user_id: 2023657,
        user_type: "registered",
        accept_rate: 67,
        profile_image:
          "https://www.gravatar.com/avatar/644e5ef3d97ee45cfeb3940ac6d60f17?s=128&d=identicon&r=PG",
        display_name: "Steve Ng",
        link: "https://stackoverflow.com/users/2023657/steve-ng"
      },
      is_answered: true,
      view_count: 10471,
      accepted_answer_id: 30546964,
      answer_count: 5,
      score: 10,
      last_activity_date: 1509599641,
      creation_date: 1432992300,
      question_id: 30546742,
      link:
        "https://stackoverflow.com/questions/30546742/react-native-how-to-move-screen-up-on-textinput",
      title: "React-native how to move screen up on textinput"
    },
    {
      tags: ["javascript", "angular", "typescript", "chatbot"],
      owner: {
        reputation: 29,
        user_id: 8724160,
        user_type: "registered",
        accept_rate: 100,
        profile_image:
          "https://www.gravatar.com/avatar/733e2778b9616ed74fa805be8014257f?s=128&d=identicon&r=PG&f=1",
        display_name: "A61NN5",
        link: "https://stackoverflow.com/users/8724160/a61nn5"
      },
      is_answered: true,
      view_count: 33,
      accepted_answer_id: 46963795,
      answer_count: 2,
      score: 1,
      last_activity_date: 1509598517,
      creation_date: 1508983069,
      last_edit_date: 1509002274,
      question_id: 46944658,
      link:
        "https://stackoverflow.com/questions/46944658/trouble-using-microsoft-botframework-webchat-in-angular-2-application",
      title:
        "Trouble Using Microsoft BotFramework-WebChat in Angular 2 Application"
    }
  ],
  has_more: true,
  quota_max: 300,
  quota_remaining: 297
};

export const questionInfo = {
  items: [
    {
      tags: ["javascript", "reactjs", "datepicker", "calendar", "material-ui"],
      owner: {
        reputation: 400,
        user_id: 476929,
        user_type: "registered",
        accept_rate: 93,
        profile_image:
          "https://www.gravatar.com/avatar/ac5936179662cf64dff8fc2f34e18ed4?s=128&d=identicon&r=PG",
        display_name: "ali",
        link: "https://stackoverflow.com/users/476929/ali"
      },
      is_answered: true,
      view_count: 525,
      accepted_answer_id: 35408783,
      answer_count: 1,
      score: 0,
      last_activity_date: 1455541098,
      creation_date: 1455356155,
      last_edit_date: 1455541098,
      question_id: 35378272,
      link:
        "https://stackoverflow.com/questions/35378272/render-full-calendar-of-datepicker-on-a-page",
      title: "Render Full Calendar of DatePicker on a page"
    }
  ],
  has_more: false,
  quota_max: 300,
  quota_remaining: 251
};

export const demoAnswers = {
  items: [
    {
      owner: {
        reputation: 446,
        user_id: 2037755,
        user_type: "registered",
        accept_rate: 76,
        profile_image: "https://i.stack.imgur.com/yyAkw.jpg?s=128&g=1",
        display_name: "Sz&#225;nt&#243; Zolt&#225;n",
        link:
          "https://stackoverflow.com/users/2037755/sz%c3%a1nt%c3%b3-zolt%c3%a1n"
      },
      is_accepted: false,
      score: 1,
      last_activity_date: 1486637059,
      creation_date: 1486637059,
      answer_id: 42134553,
      question_id: 34048467
    },
    {
      owner: {
        reputation: 421,
        user_id: 959939,
        user_type: "registered",
        accept_rate: 0,
        profile_image:
          "https://www.gravatar.com/avatar/68cc4404d2c4c5816dfbcb184c13b260?s=128&d=identicon&r=PG",
        display_name: "SenG",
        link: "https://stackoverflow.com/users/959939/seng"
      },
      is_accepted: false,
      score: 1,
      last_activity_date: 1474216158,
      creation_date: 1474216158,
      answer_id: 39560017,
      question_id: 34048467
    },
    {
      owner: {
        reputation: 1762,
        user_id: 5201742,
        user_type: "registered",
        profile_image: "https://i.stack.imgur.com/tucg0.jpg?s=128&g=1",
        display_name: "stig-js",
        link: "https://stackoverflow.com/users/5201742/stig-js"
      },
      is_accepted: false,
      score: 0,
      last_activity_date: 1449078106,
      creation_date: 1449078106,
      answer_id: 34049433,
      question_id: 34048467
    },
    {
      owner: {
        reputation: 67009,
        user_id: 3522312,
        user_type: "registered",
        profile_image:
          "https://www.gravatar.com/avatar/0c5b53060bf7f71994265a9ef7c17cc5?s=128&d=identicon&r=PG",
        display_name: "scaisEdge",
        link: "https://stackoverflow.com/users/3522312/scaisedge"
      },
      is_accepted: false,
      score: 1,
      last_activity_date: 1449077790,
      creation_date: 1449077790,
      answer_id: 34049321,
      question_id: 34048467
    }
  ],
  has_more: false,
  quota_max: 10000,
  quota_remaining: 9886
};
