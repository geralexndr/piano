This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

To install and run project you’ll need to have Node(https://nodejs.org/en/) >= 6 on your machine.

To start this project, run following commands from terminal in the root of project folder:

[npm install](#npm-install)
[npm start](#npm-start)
